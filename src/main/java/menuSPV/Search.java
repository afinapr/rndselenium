package menuSPV;

import org.openqa.selenium.By;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;

public class Search {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "D:\\Work\\79\\RND BE\\Selenium\\Apps\\chromedriver_win32\\chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://matrix-2.cloudias79.com");

		
	    if (driver.findElements(By.xpath("//p[contains(.,'Login Here !')]")).size() != 0){
	    	System.out.println("login is present. ");
	    } else {
	    	System.out.println("login is not present. ");
	    }
		driver.findElement(By.xpath("//input[@id='horizontal_login_userName']")).sendKeys("spv");
		driver.findElement(By.xpath("//input[@id='horizontal_login_password']")).sendKeys("admin");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		if(driver.findElements(By.xpath("//button[@type='submit']")).size() != 0){
			System.out.println("Element is Present");
		} else {
			System.out.println("Element is not Present");
		}
		
		
		if(driver.findElements(By.xpath("//p[contains(.,'Jumlah Data : 222')]")).size() != 0){
			System.out.println("Jumlah Data is Present");
		} else {
			System.out.println("Jumlah Data is not Present");
		}
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.cssSelector(".anticon-right > svg")).click();
		
		driver.findElement(By.cssSelector(".anticon-left > svg")).click();
		
		driver.findElement(By.xpath("//input[@id='searchInput']")).sendKeys("talent");
		driver.findElement(By.cssSelector(".anticon-search")).click();
		
		if(driver.findElements(By.xpath("//td[contains(.,'Talent')]")).size() != 0){
			System.out.println("Data is Present");
		} else {
			System.out.println("Data is not Present");
		}
	}

}
